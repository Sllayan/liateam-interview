import React from "react";
// dependencies
import { connect } from "react-redux";
import PropTypes from "prop-types";
// local
import { cropText, numberSeperator } from "../../shared/utils";
// assets
import BasketIcon from "../../assets/Svgs/basket-blue.svg";
// style
import styled from "./index.module.scss";
import { addInBasketAction } from "../../store/actions/ProductionAction";

const Products = (props) => {
  const handleBasket = (data) => {
    props._addInBasket(data);
  };

  return (
    <div className={styled.cp_pb_each_product}>
      <div className={styled.cp_pb_ec_inner_box}>
        <img
          src={"https://shopapi.liateam.com" + props.product.small_pic}
          alt="pt"
          className={styled.cp_pb_ec_image}
        />
        <div className={styled.cp_pb_ec_details_box}>
          {props.product.isNew && (
            <div className={styled.cp_pb_ec_is_new}>جدید</div>
          )}
          <p className={styled.cp_pb_ec_db_title}>
            {cropText(props.product.title, 28)}
          </p>
          <p className={styled.cp_pb_ec_db_weight}>{props.product.weight}ml</p>
          <div className={styled.cp_pb_ec_db_cost_box}>
            <p className={styled.cp_pb_ec_db_cb_cost}>
              <span className={styled.toman}>تومان</span>
              {numberSeperator(props.product.price.final_price)}
            </p>
            <img
              src={BasketIcon}
              alt="basket"
              className={styled.cp_pb_ec_db_cb_basket}
              onClick={() => handleBasket(props.product)}
            />
            <div className={styled.hover_cost}>
              <span className={styled.hover_cost_toman}>تومان</span>
              {numberSeperator(props.product.price.final_price)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Products.propTypes = {
  product: PropTypes.object,
};

const mapStateToProps = (state) => {
  return {
    addInBasket: state.product.addInBasketState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _addInBasket: (data) => dispatch(addInBasketAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
