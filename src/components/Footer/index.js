import React from "react";
// local
import Auxiliay from "../../HOC/Auxiliary/Auxiliary";
// assets
import LiaTeamLogo from "../../assets/Images/Footer/liateam-footer-logo.png";
import EmailIcon from "../../assets/Svgs/email.svg";
import CallIcon from "../../assets/Svgs/call.svg";
import TwitterIcon from "../../assets/Svgs/twitter.svg";
import InstagramIcon from "../../assets/Svgs/instagram.svg";
import GithubIcon from "../../assets/Svgs/github.svg";
import LinkedinIcon from "../../assets/Svgs/linkedin.svg";
// style
import styled from "./index.module.scss";

const Footer = (props) => {
  const SOCIAL = [
    { icon: TwitterIcon, clickEvent: "https://twitter.com/sllayan_" },
    { icon: InstagramIcon, clickEvent: "https://instagram.com/devsllayan" },
    { icon: GithubIcon, clickEvent: "https://github.com/Sllayan" },
    {
      icon: LinkedinIcon,
      clickEvent: "https://www.linkedin.com/in/shayan-hosseinali-79852b1bb/",
    },
  ];
  const INFORMATION = [
    {
      value: "sellersupport@liateam.com",
      clickEvent: "mailto:sellersupport@liateam.com",
      icon: EmailIcon,
    },
    {
      value: "021 88 88 88 88",
      clickEvent: "tel:+982188888888",
      icon: CallIcon,
    },
  ];

  return (
    <Auxiliay>
      <div className={styled.footer}>
        <div className={styled.f_top_box}>
          <div className={styled.f_tb_intro_box}>
            <img
              src={LiaTeamLogo}
              alt="liateam"
              className={styled.f_tb_ib_logo}
            />
            <p className={styled.f_tb_ib_name}>Lia Virtual Office</p>
            <p className={styled.f_tb_ib_text}>Good Time Good News</p>
          </div>

          <div className={styled.f_tb_information_box}>
            {INFORMATION.map((item, i) => {
              return (
                <div
                  key={i}
                  onClick={() => window.open(item.clickEvent, "_blank")}
                  className={[
                    styled.f_tb_ib_each_info,
                    i === 1 ? styled.border : "",
                  ].join(" ")}
                >
                  <img
                    src={item.icon}
                    alt={i}
                    className={styled.f_tb_ib_each_info_icon}
                  />
                  {item.value}
                </div>
              );
            })}
            <div className={styled.f_tb_ib_each_info}>
              مرکز پشتیبانی بازاریابان
            </div>
          </div>
        </div>

        <div className={styled.f_bottom_box}>
          <div className={styled.f_bb_law_box}>
            © تمام حقوق این وب سایت متعلق به شرکت آرمان تدبیر اطلس 1398-۱۳۹7 می
            باشد
          </div>
          <div className={styled.f_bb_social_box}>
            {SOCIAL.map((item, i) => {
              return (
                <img
                  key={i}
                  src={item.icon}
                  alt={i}
                  onClick={() => window.open(item.clickEvent, "_blank")}
                  className={styled.f_bb_sb_icon}
                />
              );
            })}
          </div>
          <div className={styled.f_bb_time_box}>
            هفت روز هفته ، ۲۴ ساعت شبانه‌روز پاسخگوی شما هستیم
          </div>
        </div>
      </div>
    </Auxiliay>
  );
};

export default Footer;
