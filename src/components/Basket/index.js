import React, { useState } from "react";
// dependencies
import { connect } from "react-redux";
// local
import Auxiliay from "../../HOC/Auxiliary/Auxiliary";
import { cropText, numberSeperator } from "../../shared/utils";
import {
  getBasketAction,
  addInBasketAction,
  subtractFromBasketAction,
  removeFromBasketAction,
} from "../../store/actions/ProductionAction";
// style
import styled from "./index.module.scss";

const Basket = (props) => {
  const [basketValue, setBasketValue] = useState(props.basket);

  const handleRemove = (data) => {
    const updated = basketValue.filter((item) => item !== data);
    setBasketValue(updated);
    props._removeFromBasket(data);
  };

  const handleAdd = (data) => {
    props._addInBasket(data);
  };

  const handleSubtract = (data) => {
    props._subtractFromBasket(data);
  };

  const showTotal = () => {
    var totalVal = 0;
    if (props.basket) {
      // eslint-disable-next-line array-callback-return
      props.basket.map((item, i) => {
        totalVal =
          parseInt(totalVal) + item.count_product * parseInt(item.price.price);
      });
    }
    return numberSeperator(totalVal);
  };

  const showFinalPrice = () => {
    var totalPrice = 0;
    if (props.basket) {
      // eslint-disable-next-line array-callback-return
      props.basket.map((item, i) => {
        totalPrice =
          parseInt(totalPrice) +
          item.count_product * parseInt(item.price.final_price);
      });
    }
    return numberSeperator(totalPrice);
  };

  return (
    <Auxiliay>
      <div className={styled.basket}>
        <div className={styled.b_basket_values_box}>
          {props.basket &&
            basketValue.map((item, i) => {
              return (
                <div key={i} className={styled.b_bvb_each_product}>
                  <div className={styled.b_bvb_ep_detail_box}>
                    <p className={styled.b_bvb_ep_db_title}>
                      {cropText(item.title, 28)}
                    </p>
                    <p className={styled.b_bvb_ep_db_cost}>
                      <span className={styled.toman}>تومان</span>
                      {numberSeperator(item.price.final_price)}
                    </p>
                    <div className={styled.b_bvb_ep_db_changing_box}>
                      <span
                        className={styled.b_bvb_ep_db_cb_delete}
                        onClick={() => handleRemove(item)}
                      >
                        &#xd7;
                      </span>
                      <div className={styled.b_bvb_ep_db_cb_number_box}>
                        <span
                          className={styled.b_bvb_ep_db_cb_nb_inner}
                          onClick={() => handleSubtract(item)}
                        >
                          -
                        </span>
                        <span className={styled.b_bvb_ep_db_cb_nb_num}>
                          {item.count_product}
                        </span>
                        <span
                          className={styled.b_bvb_ep_db_cb_nb_inner}
                          onClick={() => handleAdd(item)}
                        >
                          +
                        </span>
                      </div>
                    </div>
                  </div>
                  <img
                    src={"https://shopapi.liateam.com" + item.small_pic}
                    alt="pt"
                    className={styled.b_bvb_ep_image}
                  />
                </div>
              );
            })}
        </div>

        <div className={styled.b_detail_box}>
          <div className={styled.b_db_total}>
            <p className={styled.b_db_value}>{showTotal()}</p>
            <p className={styled.b_db_title}>: جمع کل</p>
          </div>
          <div className={styled.b_db_must_pay}>
            <p className={styled.b_db_value}>{showFinalPrice()}</p>
            <p className={styled.b_db_title}>: مبلغ قابل پرداخت</p>
          </div>
        </div>
        <div className={styled.b_button_box}>
          <button className={styled.b_bb_button}>سفارش</button>
        </div>
      </div>
    </Auxiliay>
  );
};

const mapStateToProps = (state) => {
  return {
    basket: state.product.basketState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _getBasket: () => dispatch(getBasketAction()),
    _addInBasket: (data) => dispatch(addInBasketAction(data)),
    _subtractFromBasket: (data) => dispatch(subtractFromBasketAction(data)),
    _removeFromBasket: (data) => dispatch(removeFromBasketAction(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
