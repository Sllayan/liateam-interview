import React from "react";
// dependencies
import { NavLink } from "react-router-dom";
// local
import Auxiliay from "../../../../HOC/Auxiliary/Auxiliary";
// assets
import LiaTeamLogo from "../../../../assets/Images/Toolbar/liateam-logo.png";
import UserProfile from "../../../../assets/Images/Toolbar/user-profile.jpg";
import ArrowDown from "../../../../assets/Svgs/down.svg";
// style
import styled from "./index.module.scss";

const TopToolbar = (props) => {
  //user description
  const USER = { name: "رضا پورجباری عزیز", profile: UserProfile };

  return (
    <Auxiliay>
      <div className={styled.topToolbar}>
        <div className={styled.t_user_detail_box}>
          <div className={styled.t_udb_image_box}>
            <img
              src={ArrowDown}
              alt="down"
              className={styled.t_udb_ib_down_icon}
            />
            <img
              src={USER.profile}
              alt="user-profile"
              className={styled.t_udb_ib_image}
            />
          </div>

          <div className={styled.t_udb_name_box}>
            <p className={styled.t_udb_nb_name}>{USER.name}</p>
          </div>
        </div>

        <div className={styled.t_options_box}>
          <NavLink
            to="/contact-us"
            activeClassName={styled.t_ob_option_active}
            className={styled.t_ob_option}
            exact
          >
            تماس با ما
          </NavLink>
          <NavLink
            to="/about-us"
            activeClassName={styled.t_ob_option_active}
            className={styled.t_ob_option}
            exact
          >
            درباره ما
          </NavLink>
          <NavLink
            to="/weblog"
            activeClassName={styled.t_ob_option_active}
            className={styled.t_ob_option}
            exact
          >
            وبلاگ
          </NavLink>
          <NavLink
            to="/shop"
            activeClassName={styled.t_ob_option_active}
            className={styled.t_ob_option}
            // exact
          >
            فروشگاه
          </NavLink>
          <NavLink
            to="/"
            activeClassName={styled.t_ob_option_active}
            className={styled.t_ob_option}
            exact
          >
            خانه
          </NavLink>
        </div>

        <div className={styled.t_logo_box}>
          <img src={LiaTeamLogo} alt="liateam" className={styled.t_lb_logo} />
        </div>
      </div>
    </Auxiliay>
  );
};

export default TopToolbar;
