import React, { useEffect, useState } from "react";
// dependencies
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
// local
import Auxiliay from "../../../../HOC/Auxiliary/Auxiliary";
import Basket from "../../../Basket";
// assets
import BasketIcon from "../../../../assets/Svgs/basket.svg";
// style
import styled from "./index.module.scss";

const BottomToolbar = (props) => {
  const BASKET = { count: 87 };
  const NAV_ITEM = [
    { title: "جدید ترین", code: 8888, id: "latests" },
    { title: "پرفروش ترین", code: 9999, id: "bestselling" },
    { title: "آرایشی", code: 91, id: "makeup" },
    { title: "مراقبت بدن", code: 90, id: "body" },
    { title: "مراقبت مو", code: 11, id: "hair" },
    { title: "مراقبت پوست", code: 10, id: "skin" },
  ];

  const [whichSection, setWhichSection] = useState("");
  const [basketOpen, setBasketOpen] = useState(false);

  useEffect(() => {
    if (window.location.pathname === "/shop") {
      setWhichSection("");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [window.location.pathname]);

  const handleActiveSection = (id, code) => {
    setWhichSection(id);
  };

  return (
    <Auxiliay>
      <div className={styled.bottomToolbar}>
        <div className={styled.b_basket_box}>
          <img
            src={BasketIcon}
            alt="basket"
            className={styled.b_bb_basket_icon}
            onClick={() => setBasketOpen(!basketOpen)}
          />
          <span className={styled.b_bb_value_count}>{BASKET.count}</span>
          {basketOpen && (
            <div
              className={styled.basket_box}
              onMouseLeave={() => setBasketOpen(false)}
            >
              <Basket />
            </div>
          )}
        </div>

        <div className={styled.b_bottom_navbar_box}>
          {NAV_ITEM.map((item, i) => {
            return (
              <NavLink
                key={i}
                className={styled.navlink}
                to={"/shop/category=" + item.code}
              >
                <div
                  onClick={() => handleActiveSection(item.id, item.code)}
                  className={[
                    styled.b_bnb_option,
                    whichSection === item.id &&
                    window.location.pathname.indexOf("/shop") > -1
                      ? styled.b_bnb_option_active
                      : "",
                    window.location.pathname.indexOf(item.code) > -1
                      ? styled.b_bnb_option_active
                      : "",
                  ].join(" ")}
                >
                  <span className={styled.active_motion}>{item.title}</span>
                </div>
              </NavLink>
            );
          })}
        </div>
      </div>
    </Auxiliay>
  );
};

BottomToolbar.propTypes = {
  whichSectionIsActive: PropTypes.func,
};

BottomToolbar.defaultProps = {
  whichSectionIsActive: function () {},
};

export default BottomToolbar;
