import React from "react";
// local
import Auxiliay from "../../HOC/Auxiliary/Auxiliary";
import TopToolbar from "./components/TopToolbar";
import BottomToolbar from "./components/BottomToolbar";
// style
import styled from "./index.module.scss";

const Toolbar = (props) => {
  return (
    <Auxiliay>
      <div className={styled.toolbar}>
        <TopToolbar />
        <BottomToolbar />
      </div>
    </Auxiliay>
  );
};

export default Toolbar;
