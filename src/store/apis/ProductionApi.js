import { api } from "../utility/request";

export function getCategoriesApi() {
  return api().get("/api/rest/v1/get_categories");
}

export function getProductsApi(params) {
  return api().get(
    `/api/rest/v1/get_product?categories=${params.category}?page=${params.page}`
  );
}
