import { call, put } from "redux-saga/effects";
import * as actions from "../actions/action";
import { getCategoriesApi, getProductsApi } from "../apis/ProductionApi";

export function* getCategoriesSaga(action) {
  try {
    const response = yield call(getCategoriesApi);
    yield put(actions.getCategoriesSuccessAction(response.data));
  } catch (error) {
    yield put(actions.getCategoriesFailAction(error));
  }
}
export function* getProductsSaga(action) {
  try {
    const response = yield call(getProductsApi, action.req);
    yield put(actions.getProductsSuccessAction(response.data));
  } catch (error) {
    yield put(actions.getProductsFailAction(error));
  }
}
