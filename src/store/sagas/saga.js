import { takeLatest } from "redux-saga/effects";
import * as actionTypes from "../actions/actionTypes";

import { getCategoriesSaga, getProductsSaga } from "./ProductionSaga";

export function* watchProduction() {
  yield takeLatest(actionTypes.GET_CATEGORIES, getCategoriesSaga);
  yield takeLatest(actionTypes.GET_PRODUCTS, getProductsSaga);
}
