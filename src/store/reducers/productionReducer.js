/* eslint-disable eqeqeq */
import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility/utils";

const initialState = {
  getCategoriesState: {
    req: null,
    res: null,
    err: null,
  },
  getProductsState: {
    req: null,
    res: null,
    err: null,
  },
  //
  basketState: [],
};

const getCategories = (state, action) => {
  const updateValue = {
    req: true,
    res: null,
    err: null,
  };
  return updateObject(state, { getCategoriesState: updateValue });
};

const getCategoriesSuccess = (state, action) => {
  const updateValue = {
    req: null,
    res: action.res,
    err: null,
  };
  return updateObject(state, { getCategoriesState: updateValue });
};

const getCategoriesFail = (state, action) => {
  const updateValue = {
    req: null,
    res: null,
    err: action.err,
  };
  return updateObject(state, { getCategoriesState: updateValue });
};

const getProducts = (state, action) => {
  const updateValue = {
    req: action.req,
    res: null,
    err: null,
  };
  return updateObject(state, { getProductsState: updateValue });
};

const getProductsSuccess = (state, action) => {
  const updateValue = {
    req: null,
    res: action.res,
    err: null,
  };
  return updateObject(state, { getProductsState: updateValue });
};

const getProductsFail = (state, action) => {
  const updateValue = {
    req: null,
    res: null,
    err: action.err,
  };
  return updateObject(state, { getProductsState: updateValue });
};

const getBasket = (state, action) => {
  const updateValue = initialState.basketState;
  return updateObject(state, { basketState: updateValue });
};

const addInBasket = (state, action) => {
  const updateValue = initialState.basketState;
  if (action.data.count_product) {
    for (var i in updateValue) {
      if (updateValue[i] == action.data) {
        updateValue[i].count_product = action.data.count_product + 1;
        break;
      }
    }
  } else {
    action.data.count_product = 1;
    updateValue.push(action.data);
  }
  return updateObject(state, { basketState: updateValue });
};

const subtractFromBasket = (state, action) => {
  const updateValue = initialState.basketState;
  if (action.data.count_product > 0) {
    for (var i in updateValue) {
      if (updateValue[i] == action.data) {
        updateValue[i].count_product = action.data.count_product - 1;
        break;
      }
    }
  }
  return updateObject(state, { basketState: updateValue });
};

const removeFromBasket = (state, action) => {
  const updateValue = initialState.basketState;
  const index = updateValue.indexOf(action.data);
  if (index > -1) {
    updateValue.splice(index, 1);
  }
  return updateObject(state, { basketState: updateValue });
};

const productionReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_CATEGORIES:
      return getCategories(state, action);
    case actionTypes.GET_CATEGORIES_SUCCESS:
      return getCategoriesSuccess(state, action);
    case actionTypes.GET_CATEGORIES_FAIL:
      return getCategoriesFail(state, action);

    case actionTypes.GET_PRODUCTS:
      return getProducts(state, action);
    case actionTypes.GET_PRODUCTS_SUCCESS:
      return getProductsSuccess(state, action);
    case actionTypes.GET_PRODUCTS_FAIL:
      return getProductsFail(state, action);

    case actionTypes.GET_BASKET:
      return getBasket(state, action);
    case actionTypes.ADD_IN_BASKET:
      return addInBasket(state, action);
    case actionTypes.SUBTRACT_FROM_BASKET:
      return subtractFromBasket(state, action);
    case actionTypes.REMOVE_FROM_BASKET:
      return removeFromBasket(state, action);

    default:
      return state;
  }
};

export default productionReducer;
