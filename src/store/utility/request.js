import axios from "axios";

export function api() {
  const api = axios.create({
    baseURL: "https://shopapi.liateam.com",
    // withCredentials: true,
    timeout: 10000,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });

  api.interceptors.request.use(
    function (config) {
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );
  return api;
}
