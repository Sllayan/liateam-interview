import * as actionType from "./actionTypes";

export const getCategoriesAction = () => {
  return { type: actionType.GET_CATEGORIES };
};
export const getCategoriesSuccessAction = (res) => {
  return { type: actionType.GET_CATEGORIES_SUCCESS, res };
};
export const getCategoriesFailAction = (err) => {
  return { type: actionType.GET_CATEGORIES_FAIL, err };
};

export const getProductsAction = (req) => {
  return { type: actionType.GET_PRODUCTS, req };
};
export const getProductsSuccessAction = (res) => {
  return { type: actionType.GET_PRODUCTS_SUCCESS, res };
};
export const getProductsFailAction = (err) => {
  return { type: actionType.GET_PRODUCTS_FAIL, err };
};

export const getBasketAction = () => {
  return { type: actionType.GET_BASKET };
};
export const addInBasketAction = (data) => {
  return { type: actionType.ADD_IN_BASKET, data };
};
export const subtractFromBasketAction = (data) => {
  return { type: actionType.SUBTRACT_FROM_BASKET, data };
};
export const removeFromBasketAction = (data) => {
  return { type: actionType.REMOVE_FROM_BASKET, data };
};
