import React, { useEffect } from "react";
import { connect } from "react-redux";
// local
import Auxiliay from "../../HOC/Auxiliary/Auxiliary";
import { getCategoriesAction } from "../../store/actions/action";
// assets
import ArrowLeft from "../../assets/Svgs/arrow-left.svg";
// style
import styled from "./index.module.scss";

const Shop = (props) => {
  useEffect(() => {
    props._getCategories();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const goToCategory = (id) => {
    window.location.pathname = `/shop/category=${id}`;
  };

  return (
    <Auxiliay>
      <div className={styled.shop}>
        <p className={styled.s_title}>دسته بندی ها</p>
        <div className={styled.s_categories_box}>
          {props.getcategories.req && props.getcategories.req !== null ? (
            <div className={styled.loading}>...لطفا صبر کنید</div>
          ) : (
            ""
          )}

          {props.getcategories.res && props.getcategories.res.length < 1 ? (
            <div className={styled.nothing}>.موردی وجود ندارد</div>
          ) : (
            ""
          )}

          {props.getcategories.res && props.getcategories.res.length > 0
            ? props.getcategories.res.map((item, i) => {
                return (
                  <div key={i} className={styled.s_cb_each_category}>
                    <div
                      className={styled.s_cb_ec_inner_box}
                      onClick={() => goToCategory(item.id)}
                    >
                      <img
                        src={"https://shopapi.liateam.com" + item.image}
                        alt={i}
                        className={styled.s_cb_ec_ib_image}
                      />
                      <div className={styled.s_cb_ec_ib_name}>
                        <img
                          src={ArrowLeft}
                          alt="arrow"
                          className={styled.s_cb_ec_ib_arrow_icon}
                        />
                        {item.name}
                      </div>
                    </div>
                  </div>
                );
              })
            : ""}
        </div>
      </div>
    </Auxiliay>
  );
};

const mapStateToProps = (state) => {
  return {
    getcategories: state.product.getCategoriesState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _getCategories: () => dispatch(getCategoriesAction()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Shop);
