import React from "react";
// local
import Auxiliay from "../../HOC/Auxiliary/Auxiliary";
// assets
import LiateamLogo from "../../assets/Images/Toolbar/liateam-logo.png";
// style
import styled from "./index.module.scss";

const LandPage = (props) => {
  return (
    <Auxiliay>
      <div className={styled.main_page}>
        <img src={LiateamLogo} alt="liateam" className={styled.logo} />
        <p className={styled.name}>Lia team</p>
      </div>
    </Auxiliay>
  );
};

export default LandPage;
