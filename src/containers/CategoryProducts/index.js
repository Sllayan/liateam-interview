import React, { useEffect, useState } from "react";
// dependencies
import { connect } from "react-redux";
import ReactPaginate from "react-paginate";
// local
import Auxiliay from "../../HOC/Auxiliary/Auxiliary";
import Products from "../../components/Products";
import { getProductsAction } from "../../store/actions/action";
// style
import styled from "./index.module.scss";

const CategoryProducts = (props) => {
  const [pageNumber, setPageNumber] = useState(1);

  useEffect(() => {
    props._getProducts({
      category: window.location.pathname.split("/category=")[1],
      page: pageNumber,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [window.location.pathname, pageNumber]);

  return (
    <Auxiliay>
      <div className={styled.category_products}>
        <p className={styled.cp_title}>محصولات</p>
        <div className={styled.cp_products_box}>
          {props.getProducts.req && props.getProducts.req !== null ? (
            <div className={styled.loading}>...لطفا صبر کنید</div>
          ) : (
            ""
          )}

          {props.getProducts.res &&
          props.getProducts.res.list &&
          props.getProducts.res.list.length < 1 ? (
            <div className={styled.nothing}>.موردی وجود ندارد</div>
          ) : (
            ""
          )}

          {props.getProducts.res &&
          props.getProducts.res.list &&
          props.getProducts.res.list.length > 0
            ? props.getProducts.res.list.map((item, i) => {
                return <Products key={i} product={item} />;
              })
            : ""}

          {props.getProducts.res ? (
            <div className={styled.paginate}>
              <div className={styled.paginate_box}>
                <ReactPaginate
                  pageCount={props.getProducts.res.pagecount}
                  initialPage={props.getProducts.res.page - 1}
                  pageRangeDisplayed={1}
                  marginPagesDisplayed={1}
                  previousLabel="<"
                  nextLabel=">"
                  breakClassName={styled.breakClassName}
                  breakLinkClassName={styled.breakLinkClassName}
                  containerClassName={styled.containerClassName}
                  pageClassName={styled.pageClassName}
                  pageLinkClassName={styled.pageLinkClassName}
                  activeClassName={styled.pb_activeClassName}
                  activeLinkClassName={styled.pb_activeLinkClassName}
                  previousClassName={styled.pb_previousClassName}
                  nextClassName={styled.pb_nextClassName}
                  previousLinkClassName={styled.pb_previousLinkClassName}
                  nextLinkClassName={styled.pb_nextLinkClassName}
                  disabledClassName={styled.pb_disabledClassName}
                  onPageChange={(e) => setPageNumber(e + 1)}
                />
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    </Auxiliay>
  );
};

const mapStateToProps = (state) => {
  return {
    getProducts: state.product.getProductsState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    _getProducts: (req) => dispatch(getProductsAction(req)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryProducts);
