import React from "react";
// dependencies
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
// local
import Layout from "./HOC/Layout";
import LandPage from "./containers/LandPage";
import Shop from "./containers/Shop";
import CategoryProducts from "./containers/CategoryProducts";

const App = (props) => {
  let routes = (
    <Switch>
      <Route path="/shop" render={(props) => <Shop {...props} />} exact />
      <Route
        path="/shop/category=:id"
        render={(props) => <CategoryProducts {...props} />}
      />
      <Route path="/" render={(props) => <LandPage {...props} />} />
      <Redirect to="/" />
    </Switch>
  );

  return (
    <Layout toolbar={true} footer={true}>
      {routes}
    </Layout>
  );
};

export default withRouter(App);
