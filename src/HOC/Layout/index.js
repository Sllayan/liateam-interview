import React from "react";
// dependencies
import PropTypes from "prop-types";
// local
import Auxiliay from "../Auxiliary/Auxiliary";
import Toolbar from "../../components/Toolbar";
import Footer from "../../components/Footer";
// style
import styled from "./index.module.scss";

const Layout = (props) => {
  return (
    <Auxiliay>
      <div className={styled.layout}>
        {props.toolbar && <Toolbar />}
        <main className={styled.main}>{props.children}</main>
        {props.footer && <Footer />}
      </div>
    </Auxiliay>
  );
};

Layout.propTypes = {
  toolbar: PropTypes.bool,
};

Layout.defaultProps = {
  toolbar: true,
};

export default Layout;
