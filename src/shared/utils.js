export const cropText = (text, number = 20) => {
  if (text.length < number) {
    return text;
  } else {
    return "..." + text.substring(0, 20);
  }
};

export const numberSeperator = (num) => {
  if (num.toString().length <= 3) {
    return num;
  } else if (num.toString().length <= 6) {
    return num.toString().slice(-6, -3) + "," + num.toString().slice(-3);
  } else if (num.toString().length <= 9) {
    return (
      num.toString().slice(-9, -6) +
      "," +
      num.toString().slice(-6, -3) +
      "," +
      num.toString().slice(-3)
    );
  } else if (num.toString().length <= 12) {
    return (
      num.toString().slice(-12, -9) +
      "," +
      num.toString().slice(-9, -6) +
      "," +
      num.toString().slice(-6, -3) +
      "," +
      num.toString().slice(-3)
    );
  }
};
